from datetime import datetime

import ccxt.pro

from Analysis import Analysis
from helpers.utils import debug_log


class TriArb:
    def __init__(self, exchange):
        self.exchange = exchange
        self.analysis = Analysis(exchange)

    async def place_trade_orders(self, arb_type, scrip1, scrip2, scrip3, initial_amount, scrip_prices):
        final_amount = 0.0
        if arb_type == 'BUY_BUY_SELL':
            s1_quantity = initial_amount / scrip_prices[scrip1]
            await self.exchange.place_buy_order(scrip1, s1_quantity, scrip_prices[scrip1])

            s2_quantity = s1_quantity / scrip_prices[scrip2]
            await self.exchange.place_buy_order(scrip2, s2_quantity, scrip_prices[scrip2])

            s3_quantity = s2_quantity
            await self.exchange.place_sell_order(scrip3, s3_quantity, scrip_prices[scrip3])

        elif arb_type == 'BUY_SELL_SELL':
            s1_quantity = initial_amount / scrip_prices[scrip1]
            await self.exchange.place_buy_order(scrip1, s1_quantity, scrip_prices[scrip1])

            s2_quantity = s1_quantity
            await self.exchange.place_sell_order(scrip2, s2_quantity, scrip_prices[scrip2])

            s3_quantity = s2_quantity * scrip_prices[scrip2]
            await self.exchange.place_sell_order(scrip3, s3_quantity, scrip_prices[scrip3])

        return final_amount

    async def perform_triangular_arbitrage(self, scrip1, scrip2, scrip3, arbitrage_type, initial_investment,
                                           transaction_brokerage, min_profit):
        scrip_prices = {}
        final_price = 0.0
        if arbitrage_type == 'BUY_BUY_SELL':
            # Check this combination for triangular arbitrage: scrip1 - BUY, scrip2 - BUY, scrip3 - SELL
            final_price, scrip_prices = await self.analysis.check_buy_buy_sell(
                scrip1, scrip2, scrip3, initial_investment)

        elif arbitrage_type == 'BUY_SELL_SELL':
            # Check this combination for triangular arbitrage: scrip1 - BUY, scrip2 - SELL, scrip3 - SELL
            final_price, scrip_prices = await self.analysis.check_buy_sell_sell(
                scrip1, scrip2, scrip3, initial_investment)

        profit_loss = self.analysis.check_profit_loss(final_price, initial_investment,
                                                      transaction_brokerage, min_profit)

        if profit_loss > 0:
            debug_log(f"PROFIT-{datetime.now().strftime('%H:%M:%S')}:"
                      f"{arbitrage_type}, {scrip1}, {scrip2}, {scrip3}, "
                      f"Profit/Loss: {round(final_price - initial_investment, 3)} ")

            # UNCOMMENT THIS LINE TO PLACE THE ORDERS
            await self.place_trade_orders(arbitrage_type, scrip1, scrip2, scrip3, initial_investment, scrip_prices)

