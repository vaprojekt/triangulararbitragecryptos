import math
# import time

# import ccxt.pro


class Analysis:
    def __init__(self, exchange):
        self.exchange = exchange

    @staticmethod
    def check_if_float_zero(value):
        return math.isclose(value, 0.0, abs_tol=1e-3)

    # forward arbitrage
    async def check_buy_buy_sell(self, scrip1, scrip2, scrip3, initial_investment):

        # SCRIP1
        investment_amount1 = initial_investment
        current_price1 = await self.exchange.fetch_current_ticker_price(scrip1)
        final_price = 0
        scrip_prices = {}

        if current_price1 is not None and not self.check_if_float_zero(current_price1):
            buy_quantity1 = round(investment_amount1 / current_price1, 8)

            # TRY WITHOUT SLEEP IF THE EXCHANGE DOES NOT THROW RATE LIMIT EXCEPTIONS
            # time.sleep(1)
            # SCRIP2
            investment_amount2 = buy_quantity1
            current_price2 = await self.exchange.fetch_current_ticker_price(scrip2)
            if current_price2 is not None and not self.check_if_float_zero(current_price2):
                buy_quantity2 = round(investment_amount2 / current_price2, 8)

                # TRY WITHOUT SLEEP IF THE EXCHANGE DOES NOT THROW RATE LIMIT EXCEPTIONS
                # time.sleep(1)
                # SCRIP3
                investment_amount3 = buy_quantity2
                current_price3 = await self.exchange.fetch_current_ticker_price(scrip3)
                if current_price3 is not None and not self.check_if_float_zero(current_price3):
                    sell_quantity3 = buy_quantity2
                    final_price = round(sell_quantity3 * current_price3, 3)
                    scrip_prices = {scrip1: current_price1, scrip2: current_price2, scrip3: current_price3}

        return final_price, scrip_prices

    # reserve arbitrage
    async def check_buy_sell_sell(self, scrip1, scrip2, scrip3, initial_investment):
        # SCRIP1
        investment_amount1 = initial_investment
        current_price1 = await self.exchange.fetch_current_ticker_price(scrip1)
        final_price = 0
        scrip_prices = {}
        if current_price1 is not None and not self.check_if_float_zero(current_price1):
            buy_quantity1 = round(investment_amount1 / current_price1, 8)

            # TRY WITHOUT SLEEP IF THE EXCHANGE DOES NOT THROW RATE LIMIT EXCEPTIONS
            # time.sleep(1)
            # SCRIP2
            investment_amount2 = buy_quantity1
            current_price2 = await self.exchange.fetch_current_ticker_price(scrip2)
            if current_price2 is not None and not self.check_if_float_zero(current_price2):
                sell_quantity2 = buy_quantity1
                sell_price2 = round(sell_quantity2 * current_price2, 8)

                # TRY WITHOUT SLEEP IF THE EXCHANGE DOES NOT THROW RATE LIMIT EXCEPTIONS
                # time.sleep(1)
                # SCRIP1
                investment_amount3 = sell_price2
                current_price3 = await self.exchange.fetch_current_ticker_price(scrip3)
                if current_price3 is not None and not self.check_if_float_zero(current_price3):
                    sell_quantity3 = sell_price2
                    final_price = round(sell_quantity3 * current_price3, 3)
                    scrip_prices = {scrip1: current_price1, scrip2: current_price2, scrip3: current_price3}
        return final_price, scrip_prices

    @staticmethod
    def check_profit_loss(total_price_after_sell, initial_investment, transaction_brokerage, min_profit):
        apprx_brokerage = transaction_brokerage * initial_investment / 100 * 3
        min_profitable_price = initial_investment + apprx_brokerage + min_profit
        profit_loss = round(total_price_after_sell - min_profitable_price, 3)
        return profit_loss
