import inspect


def debug_log(message):
    frame = inspect.currentframe().f_back
    class_name = frame.f_globals.get('__name__', '__main__')
    method_name = frame.f_code.co_name
    print(f"[DEBUG] [{class_name}.{method_name}] {message}")


def filter_symbols(market_symbols, top_symbols):
    market_symbols_filtered = []
    for sym1 in market_symbols:
        sym1_token1, sym1_token2 = sym1.split('/')
        if sym1_token1 in top_symbols and sym1_token2 in top_symbols:
            market_symbols_filtered.append(sym1)
    return market_symbols_filtered


def get_crypto_combinations(market_symbols, base):
    combinations = []
    for sym1 in market_symbols:
        sym1_token1, sym1_token2 = sym1.split('/')
        if sym1_token2 == base:
            for sym2 in market_symbols:
                sym2_token1, sym2_token2 = sym2.split('/')
                if sym1_token1 == sym2_token2:
                    for sym3 in market_symbols:
                        sym3_token1, sym3_token2 = sym3.split('/')
                        if sym2_token1 == sym3_token1 and sym3_token2 == sym1_token2:
                            combination = {
                                'base': sym1_token2,
                                'intermediate': sym1_token1,
                                'ticker': sym2_token1,
                            }
                            combinations.append(combination)
    return combinations
