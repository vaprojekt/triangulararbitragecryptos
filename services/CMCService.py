import requests
from config import config


class CMCService:
    def __init__(self):
        self.api_key = config.get_config_value(parameter="CMC_PRO_API_KEY")
        self.num_of_top_coin = config.get_config_value(parameter="NUM_OF_TOP_COIN_IN_CMC")

    def get_top_symbols(self):
        url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest'
        parameters = {
            'start': '1',
            'limit': self.num_of_top_coin,
            'convert': 'USD',
            'sort': 'market_cap',
            'sort_dir': 'desc'
        }
        headers = {
            'Accepts': 'application/json',
            'X-CMC_PRO_API_KEY': self.api_key
        }
        response = requests.get(url, headers=headers, params=parameters)
        data = response.json()

        if response.status_code == 200:
            return [crypto['symbol'] for crypto in data['data']]
        else:
            print(f"Error: {response.status_code}, {data['status']['error_message']}")
            return []