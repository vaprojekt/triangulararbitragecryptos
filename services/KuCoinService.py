import asyncio
import traceback
import ccxt.pro
from config import config
import logging

from helpers.utils import debug_log

logger = logging.getLogger(__name__)


class KuCoinService:
    def __init__(self):
        self.exchange = self.init_exchange()

    @staticmethod
    def init_exchange():
        cf = config.load_config()
        api_key = cf["KUCOIN_YOUR_API_KEY"]
        api_secret = cf["KUCOIN_YOUR_SECRET"]
        api_passphrase = cf["KUCOIN_YOUR_PASS"]

        exchange = ccxt.pro.kucoin({
            "apiKey": api_key,
            "secret": api_secret,
            "password": api_passphrase,
        })
        return exchange

    def get_exchange(self):
        return self.exchange

    async def fetch_market_symbols(self):
        while True:
            try:
                if not self.exchange:
                    self.exchange = self.init_exchange()

                markets = await self.exchange.fetchMarkets()
                return [market['symbol'] for market in markets]
            except Exception as e:
                logger.error(f"Error fetching ticker data: {str(e)}")
                traceback.print_exc()
                await asyncio.sleep(10)  # Retry after 10 seconds in case of error
            finally:
                await self.exchange.close()

    # method to fetch the current ticker price
    async def fetch_current_ticker_price(self, ticker):
        while True:
            try:
                if not self.exchange:
                    self.exchange = self.init_exchange()

                current_ticker_details = await self.exchange.fetch_ticker(ticker)
                ticker_price = current_ticker_details['close'] if current_ticker_details is not None else None
                return ticker_price
            except Exception as e:
                logger.error(f"Error fetching ticker data: {str(e)}")
                traceback.print_exc()
                await asyncio.sleep(10)  # Retry after 10 seconds in case of error
            finally:
                await self.exchange.close()

    async def place_buy_order(self, scrip, quantity, limit):
        debug_log(f"Trying to buy {quantity} {scrip} at rate {limit}")
        order = await self.exchange.create_limit_buy_order(scrip, quantity, limit, params={
            "timeInForce": "FOK",
            "remark": f"buy_{scrip}",
            "stp": "CN",
            "tradeType": "TRADE",
        })
        return order

    async def place_sell_order(self, scrip, quantity, limit):
        debug_log(f"Trying to sell {quantity} {scrip} at rate {limit}")
        order = await self.exchange.create_limit_sell_order(scrip, quantity, limit, params={
            "timeInForce": "FOK",
            "remark": f"sell_{scrip}",
            "stp": "CN",
            "tradeType": "TRADE",
        })
        return order
