import os
import yaml


def load_config():
    """Load the configuration yaml and return dictionary of setttings.

    Returns:
        yaml as a dictionary.
    """
    config_path = os.path.join(os.path.dirname(__file__), "parameters.yaml")
    with open(config_path, "r") as config_file:
        config_defs = yaml.safe_load(config_file.read())

    if config_defs.values() is None:
        raise ValueError("parameters yaml file incomplete")

    return config_defs


def get_config_value(parameter=""):
    cf = load_config()
    value = cf[f"{parameter}"]
    return value
