import asyncio
import logging
import traceback

import pandas as pd
import ccxt.pro
from config import config
from TriArb import TriArb
from helpers.utils import debug_log
from helpers import utils
from services.CMCService import CMCService
from services.KuCoinService import KuCoinService

# logging.basicConfig(level=logging.INFO, format='%(levelname)s: %(message)s')
logger = logging.getLogger(__name__)
logger.info('CCXT version', ccxt.pro.__version__)


async def main():
    kc_service = KuCoinService()
    cmc_service = CMCService()
    tri_arb = TriArb(kc_service)

    investment_amount = config.get_config_value('fiat_cost_per_trade')
    # profit in usd
    min_profit = config.get_config_value('min_profit_in_fiat')
    brokerage_per_transaction_pct = config.get_config_value('brokerage_per_transaction_percent')
    # wx_combinations_usdt = []

    async def prepare_tickers(queue):
        # Load all ticker
        market_symbols = await kc_service.fetch_market_symbols()
        debug_log(f'No. of market symbols: {len(market_symbols)}')

        # Get the highest symbols in CMC.
        top_symbols = cmc_service.get_top_symbols()
        debug_log(f'No. of top symbols: {len(top_symbols)}')

        # Symbol filtered coinmarketcap
        market_symbols_filtered = utils.filter_symbols(market_symbols, top_symbols)
        debug_log(f'No. of filtered market symbols: {len(market_symbols_filtered)}')

        # Get all the crypto combinations for usdt, add another base if need
        wx_combinations_usdt = utils.get_crypto_combinations(market_symbols_filtered, 'USDT')
        debug_log(f'No. of crypto combinations: {len(wx_combinations_usdt)}')

        cominations_df = pd.DataFrame(wx_combinations_usdt)
        debug_log(cominations_df.head())

        await queue.put(wx_combinations_usdt)

    async def execute_tri_arb(queue):
        while True:
            try:
                wx_combinations_usdt = await queue.get()
                for combination in wx_combinations_usdt:
                    base = combination['base']
                    intermediate = combination['intermediate']
                    ticker = combination['ticker']

                    s1 = f'{intermediate}/{base}'  # Eg: BTC/USDT
                    s2 = f'{ticker}/{intermediate}'  # Eg: ETH/BTC
                    s3 = f'{ticker}/{base}'  # Eg: ETH/USDT
                    # debug_log(f's1: {s1} s2: {s2} s3: {s3}')
                    # Check triangular arbitrage for buy-buy-sell
                    await tri_arb.perform_triangular_arbitrage(s1, s2, s3, 'BUY_BUY_SELL', investment_amount,
                                                               brokerage_per_transaction_pct, min_profit)
                    # Check triangular arbitrage for buy-sell-sell
                    await tri_arb.perform_triangular_arbitrage(s3, s2, s1, 'BUY_SELL_SELL', investment_amount,
                                                               brokerage_per_transaction_pct, min_profit)
            except Exception as e:
                logger.error(f"Error execute_tri_arb: {str(e)}")
                traceback.print_exc()
                await asyncio.sleep(1)  # Retry after 10 seconds in case of error
            # finally:
            #       await exchange.close()

    queue = asyncio.Queue()
    """Main coroutine to fetch ticker data, account balance, and trade orders."""
    await asyncio.gather(
        prepare_tickers(queue),
        execute_tri_arb(queue)
        # watch_ticker_all(),
    )
    # Use queue for refactored version, logger in each class instead of in Log


if __name__ == "__main__":
    asyncio.run(main())
